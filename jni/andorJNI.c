#include <stdlib.h>
#include <math.h>
//#include <stdio.h>
//#include <string.h>
#include <exception>

//JNI definition (and jni.h)
#include "andorJNI_AndorSDK.h"
#include "atcore.h"

// An extra for AndorSDKException
#define JNI_LAYER_ERROR -543


/* Check return code from AndorSDK functions and throw exception if necessary */
void checkReturn(JNIEnv *env, int returnCode){
	if(returnCode == AT_SUCCESS)
		return; //all OK


	// Find and instantiate the exception
	jclass exClass = env->FindClass("andorJNI/AndorSDKException");

	jmethodID constructor = env->GetMethodID(exClass, "<init>", "(I)V");

	jobject exception = env->NewObject( exClass, constructor, returnCode);

	// Throw the exception. Since this is native code,
	// execution continues, and the execution will be abruptly
	// interrupted at the point in time when we return to the VM. 
	// The calling code will perform the early return back to Java code.
	env->Throw( (jthrowable) exception);

	// Clean up local reference
	env->DeleteLocalRef( exClass);
}


/* getWideCharString(): Convert java strings (UTF8) to wide char format used by AndorSDK.
 * Don't forget to free(ret) after use!!
 */
wchar_t *getWideCharString(JNIEnv *env, jstring jStr){
 	int strLen = env->GetStringLength(jStr);
	int maxBytes = (strLen + 2)* sizeof(wchar_t);

	wchar_t *wStr = (wchar_t*)malloc(maxBytes);
	if(wStr == NULL)
		checkReturn(env, JNI_LAYER_ERROR); //throws AndorSDKException(JNI_LAYER_ERROR)
	
	const char *cStr = env->GetStringUTFChars(jStr, NULL);
	if (cStr == NULL)
		return NULL;
	
	int n = mbstowcs(wStr, cStr, strLen+1);
	if(n != strLen){
		fprintf(stderr, "WARNING: mbstowcs() converted %i chars of string '%s'.\n", n, cStr);
	}
	
	env->ReleaseStringUTFChars(jStr, cStr);  // release resources
 
	return wStr;
}


JNIEXPORT void JNICALL Java_andorJNI_AndorSDK_AT_1InitialiseLibrary
  (JNIEnv *env, jclass cls){
	try{
		checkReturn(env, AT_InitialiseLibrary());

	}catch(const std::exception& ex){
		printf("AT_InitialiseLibrary\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_InitialiseLibrary\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}

}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_FinaliseLibrary
 * Signature: ()I
 */
JNIEXPORT void JNICALL Java_andorJNI_AndorSDK_AT_1FinaliseLibrary
  (JNIEnv *env, jclass cls){
	try{
		checkReturn(env, AT_FinaliseLibrary());

	}catch(const std::exception& ex){
		printf("STD EXCEPTION AT_FinaliseLibrary\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_FinaliseLibrary\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_Open
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_andorJNI_AndorSDK_AT_1Open
  (JNIEnv *env, jclass cls, jint deviceID){
	AT_H atHandle = 0;
	try{
		int returnCode = AT_Open(deviceID, &atHandle);
		checkReturn(env, returnCode);


	}catch(const std::exception& ex){
		printf("STD EXCEPTION AT_Open\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_Open\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
	return atHandle;
}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_Close
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_andorJNI_AndorSDK_AT_1Close
  (JNIEnv *env, jclass cls, jint handle){
	try{
		int returnCode = AT_Close(handle);
		checkReturn(env, returnCode);

	}catch(const std::exception& ex){
		printf("STD EXCEPTION AT_Close\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_Close\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_IsImplemented
 * Signature: (I[B)Z
 */
JNIEXPORT jboolean JNICALL Java_andorJNI_AndorSDK_AT_1IsImplemented
  (JNIEnv *env, jclass cls, jint handle, jstring jFeature){
	AT_BOOL atBool = 0;
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_IsImplemented(handle, wcFeature, &atBool);
		free(wcFeature);
		checkReturn(env, returnCode);


	}catch(const std::exception& ex){
		printf("STD EXCEPTION AT_IsImplemented\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_IsImplemented\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
	return atBool;
}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_IsReadable
 * Signature: (I[B)Z
 */
JNIEXPORT jboolean JNICALL Java_andorJNI_AndorSDK_AT_1IsReadable
 (JNIEnv *env, jclass cls, jint handle, jstring jFeature){
	AT_BOOL atBool = 0;
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_IsReadable(handle, wcFeature, &atBool);
		free(wcFeature);
		checkReturn(env, returnCode);


	}catch(const std::exception& ex){
		printf("STD EXCEPTION AT_IsReadable\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_IsReadable\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
	return atBool;
}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_IsWritable
 * Signature: (I[B)Z
 */
JNIEXPORT jboolean JNICALL Java_andorJNI_AndorSDK_AT_1IsWritable
   (JNIEnv *env, jclass cls, jint handle, jstring jFeature){
	AT_BOOL atBool = 0;
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_IsWritable(handle, wcFeature, &atBool);
		free(wcFeature);
		checkReturn(env, returnCode);


	}catch(const std::exception& ex){
		printf("STD EXCEPTION AT_IsWritable\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_IsWritable\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
	return atBool;
}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_IsReadOnly
 * Signature: (I[B)Z
 */
JNIEXPORT jboolean JNICALL Java_andorJNI_AndorSDK_AT_1IsReadOnly
  (JNIEnv *env, jclass cls, jint handle, jstring jFeature){
	AT_BOOL atBool = 0;
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_IsReadOnly(handle, wcFeature, &atBool);
		free(wcFeature);
		checkReturn(env, returnCode);

	}catch(const std::exception& ex){
		printf("STD EXCEPTION AT_IsReadOnly\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_IsReadOnly\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
	return atBool;
}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_SetInt
 * Signature: (I[BJ)V
 */
JNIEXPORT void JNICALL Java_andorJNI_AndorSDK_AT_1SetInt
  (JNIEnv *env, jclass cls, jint handle, jstring jFeature, jlong value){
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_SetInt(handle, wcFeature, value);
		free(wcFeature);
		checkReturn(env, returnCode);

	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_SetInt\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_SetInt\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_GetInt
 * Signature: (I[B)I
 */
JNIEXPORT jlong JNICALL Java_andorJNI_AndorSDK_AT_1GetInt
  (JNIEnv *env, jclass cls, jint handle, jstring jFeature){
	AT_64 atValue = 0;
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_GetInt(handle, wcFeature, &atValue);
		free(wcFeature);
		checkReturn(env, returnCode);

	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_GetInt\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_GetInt\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
	return atValue;
}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_GetIntMax
 * Signature: (I[B)I
 */
JNIEXPORT jlong JNICALL Java_andorJNI_AndorSDK_AT_1GetIntMax
  (JNIEnv *env, jclass cls, jint handle, jstring jFeature){
	AT_64 atValue = 0;
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_GetIntMax(handle, wcFeature, &atValue);
		free(wcFeature);
		checkReturn(env, returnCode);

	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_GetIntMax\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_GetIntMax\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
	return atValue;
}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_GetIntMin
 * Signature: (I[B)I
 */
JNIEXPORT jlong JNICALL Java_andorJNI_AndorSDK_AT_1GetIntMin
  (JNIEnv *env, jclass cls, jint handle, jstring jFeature){
	AT_64 atValue = 0;
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_GetIntMin(handle, wcFeature, &atValue);
		free(wcFeature);
		checkReturn(env, returnCode);


	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_Command\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN \n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
	return atValue;
}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_SetFloat
 * Signature: (ILjava/lang/String;D)V
 */
JNIEXPORT void JNICALL Java_andorJNI_AndorSDK_AT_1SetFloat
   (JNIEnv *env, jclass cls, jint handle, jstring jFeature, jdouble value){
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_SetFloat(handle, wcFeature, value);
		free(wcFeature);
		checkReturn(env, returnCode);

	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_Command\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN \n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_GetFloat
 * Signature: (ILjava/lang/String;)D
 */
JNIEXPORT jdouble JNICALL Java_andorJNI_AndorSDK_AT_1GetFloat
 (JNIEnv *env, jclass cls, jint handle, jstring jFeature){
	double value = NAN;
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_GetFloat(handle, wcFeature, &value);
		free(wcFeature);
		checkReturn(env, returnCode);

	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_GetFloat\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_GetFloat\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
	return value;
}


/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_GetFloatMax
 * Signature: (ILjava/lang/String;)D
 */
JNIEXPORT jdouble JNICALL Java_andorJNI_AndorSDK_AT_1GetFloatMax
 (JNIEnv *env, jclass cls, jint handle, jstring jFeature){
	double value = NAN;
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_GetFloatMax(handle, wcFeature, &value);
		free(wcFeature);
		checkReturn(env, returnCode);

	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_GetFloatMax\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_GetFloatMax\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
	return value;
}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_GetFloatMin
 * Signature: (ILjava/lang/String;)D
 */
JNIEXPORT jdouble JNICALL Java_andorJNI_AndorSDK_AT_1GetFloatMin
 (JNIEnv *env, jclass cls, jint handle, jstring jFeature){
	double value = NAN;
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_GetFloatMin(handle, wcFeature, &value);
		free(wcFeature);
		checkReturn(env, returnCode);

	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_GetFloatMin\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_GetFloatMin\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
	return value;
}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_SetBool
 * Signature: (ILjava/lang/String;Z)V
 */
JNIEXPORT void JNICALL Java_andorJNI_AndorSDK_AT_1SetBool
  (JNIEnv *env, jclass cls, jint handle, jstring jFeature, jboolean value){
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_SetBool(handle, wcFeature, value);
		free(wcFeature);
		checkReturn(env, returnCode);


	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_Command\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN \n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
}
/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_GetBool
 * Signature: (ILjava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_andorJNI_AndorSDK_AT_1GetBool
  (JNIEnv *env, jclass cls, jint handle, jstring jFeature){
	AT_BOOL value = 0;
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_GetBool(handle, wcFeature, &value);
		free(wcFeature);
		checkReturn(env, returnCode);

	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_GetBool\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_GetBool\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
	return value;
}



/* Class:     andorJNI_AndorSDK
 * Method:    AT_SetString
 * Signature: (ILjava/lang/String;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_andorJNI_AndorSDK_AT_1SetString
 (JNIEnv *env, jclass cls, jint handle, jstring jFeature, jstring jStr){
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		wchar_t *wcStr = getWideCharString(env, jStr);
		int returnCode = AT_SetString(handle, wcFeature, wcStr);
		free(wcFeature);
		free(wcStr);
		checkReturn(env, returnCode);

	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_SetString\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_SetString\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_GetString
 * Signature: (ILjava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_andorJNI_AndorSDK_AT_1GetString
  (JNIEnv *env, jclass cls, jint handle, jstring jFeature){
	try{
		int maxLen = 0;
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_GetStringMaxLength(handle, wcFeature, &maxLen);
		free(wcFeature);
		checkReturn(env, returnCode);

		wchar_t *wcStr = (wchar_t *)malloc((maxLen + 2) * sizeof(wchar_t));
		if(wcStr == NULL)
			checkReturn(env, JNI_LAYER_ERROR);
		
		wcFeature = getWideCharString(env, jFeature);
		returnCode = AT_GetString(handle, wcFeature, wcStr, maxLen);
		free(wcFeature);
		checkReturn(env, returnCode);
		
		char *cStr = (char *)malloc((maxLen + 2));
		if(cStr == NULL)
			checkReturn(env, JNI_LAYER_ERROR);
		wcstombs(cStr, wcStr, maxLen);	
		free(wcStr);

		jstring jStr = env->NewStringUTF(cStr);
		if(jStr == NULL)
			checkReturn(env, JNI_LAYER_ERROR);
		return jStr;

	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_GetString\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_GetString\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
	return NULL;
}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_GetStringMaxLength
 * Signature: (ILjava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_andorJNI_AndorSDK_AT_1GetStringMaxLength
  (JNIEnv *env, jclass cls, jint handle, jstring jFeature){
	int value = 0;
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_GetStringMaxLength(handle, wcFeature, &value);
		free(wcFeature);
		checkReturn(env, returnCode);

	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_GetStringMaxLength\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_GetStringMaxLength\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
	return value;
}


/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_Command
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_andorJNI_AndorSDK_AT_1Command
  (JNIEnv *env, jclass cls, jint handle, jstring jFeature){
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		printf("AT_Command\n"); fflush(stdout);
		int returnCode = AT_Command(handle, wcFeature);
		free(wcFeature);
		checkReturn(env, returnCode);

	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_Command\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_Command\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_QueueBuffer
 * Signature: (ILjava/nio/ByteBuffer;I)V
 */
JNIEXPORT jlong JNICALL Java_andorJNI_AndorSDK_AT_1QueueBuffer
  (JNIEnv *env, jclass cls, jint handle, jobject jByteBuffer, jint size){
	jbyte *cBufferPtr = NULL;
	try{

		cBufferPtr = (jbyte*)env->GetDirectBufferAddress(jByteBuffer);
		long bufferLen = env->GetDirectBufferCapacity(jByteBuffer);

		if(cBufferPtr == NULL || bufferLen < size){
			fprintf(stderr, "AndorJNI: AT_QueueBuffer(): Byte buffer to be queued is at %p, length = %li, but  "
				"%i bytes were given as size. Object ptr = %p\n", cBufferPtr, bufferLen, size, jByteBuffer);
			fflush(stderr);
			checkReturn(env, JNI_LAYER_ERROR); //throw AndorSDKException(JNI_LAYER_ERROR)
		}

		int returnCode = AT_QueueBuffer(handle, (AT_U8 *)cBufferPtr, bufferLen);
		checkReturn(env, returnCode);


	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_QueueBuffer\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_QueueBuffer\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}

	return (jlong)cBufferPtr;
}


/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_WaitBuffer
 * Signature: (I[II)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jlong JNICALL Java_andorJNI_AndorSDK_AT_1WaitBuffer
  (JNIEnv *env, jclass cls, jint handle, jint timeout){

	AT_U8 *buffAddr = NULL;
	try{
		int buffSize = -1;
		int returnCode = AT_WaitBuffer(handle, &buffAddr, &buffSize, timeout);
		checkReturn(env, returnCode);	

	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_WaitBuffer\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_WaitBuffer\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}

	return (jlong)buffAddr;
}


/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_Flush
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_andorJNI_AndorSDK_AT_1Flush
  (JNIEnv *env, jclass cls, jint handle){
	try{
		int returnCode = AT_Flush(handle);
		checkReturn(env, returnCode);

	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_Flush\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_Flush\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
}



/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_SetEnumIndex
 * Signature: (ILjava/lang/String;I)V
 */
JNIEXPORT void JNICALL Java_andorJNI_AndorSDK_AT_1SetEnumIndex
  (JNIEnv *env, jclass cls, jint handle, jstring jFeature, jint value){
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_SetEnumIndex(handle, wcFeature, value);
		free(wcFeature);
		checkReturn(env, returnCode);

	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_SetEnumIndex\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_SetEnumIndex\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
}


/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_SetEnumString
 * Signature: (ILjava/lang/String;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_andorJNI_AndorSDK_AT_1SetEnumString
  (JNIEnv *env, jclass cls, jint handle, jstring jFeature, jstring jStr){
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		wchar_t *wcStr = getWideCharString(env, jStr);
		int returnCode = AT_SetEnumString(handle, wcFeature, wcStr);
		free(wcFeature);
		free(wcStr);
		checkReturn(env, returnCode);

	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_SetEnumString\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_SetEnumString\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_GetEnumIndex
 * Signature: (ILjava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_andorJNI_AndorSDK_AT_1GetEnumIndex
  (JNIEnv *env, jclass cls, jint handle, jstring jFeature){
	int value = -1;
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_GetEnumIndex(handle, wcFeature, &value);
		free(wcFeature);
		checkReturn(env, returnCode);

	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_Command\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN \n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
	return value;
}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_GetEnumCount
 * Signature: (ILjava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_andorJNI_AndorSDK_AT_1GetEnumCount
  (JNIEnv *env, jclass cls, jint handle, jstring jFeature){
	int value = -1;
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_GetEnumCount(handle, wcFeature, &value);
		free(wcFeature);
		checkReturn(env, returnCode);

	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_GetEnumCount\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_GetEnumCount\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
	return value;
}
/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_IsEnumIndexAvailable
 * Signature: (ILjava/lang/String;I)Z
 */
JNIEXPORT jboolean JNICALL Java_andorJNI_AndorSDK_AT_1IsEnumIndexAvailable
  (JNIEnv *env, jclass cls, jint handle, jstring jFeature, jint index){
	AT_BOOL value = 0;
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_IsEnumIndexAvailable(handle, wcFeature, index, &value);
		free(wcFeature);
		checkReturn(env, returnCode);

	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_IsEnumIndexAvailable\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_IsEnumIndexAvailable\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
	return value;
}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_IsEnumIndexImplemented
 * Signature: (ILjava/lang/String;I)Z
 */
JNIEXPORT jboolean JNICALL Java_andorJNI_AndorSDK_AT_1IsEnumIndexImplemented
  (JNIEnv *env, jclass cls, jint handle, jstring jFeature, jint index){
	AT_BOOL value = 0;
	try{
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_IsEnumIndexImplemented(handle, wcFeature, index, &value);
		free(wcFeature);
		checkReturn(env, returnCode);

	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_IsEnumIndexImplemented\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_IsEnumIndexImplemented\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
	return value;
}

/*
 * Class:     andorJNI_AndorSDK
 * Method:    AT_GetEnumStringByIndex
 * Signature: (ILjava/lang/String;I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_andorJNI_AndorSDK_AT_1GetEnumStringByIndex
  (JNIEnv *env, jclass cls, jint handle, jstring jFeature, jint index){

	try{
		/* // doesn't work for enum
		int maxLen; 
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_GetStringMaxLength(handle, wcFeature, &maxLen);
		free(wcFeature);
		checkReturn(env, returnCode);
		*/
		int maxLen = 512; //so where should be get this from???

		wchar_t *wcStr = (wchar_t *)malloc((maxLen + 2) * sizeof(wchar_t));
		if(wcStr == NULL)
			checkReturn(env, JNI_LAYER_ERROR);
		
		wchar_t *wcFeature = getWideCharString(env, jFeature);
		int returnCode = AT_GetEnumStringByIndex(handle, wcFeature, index, wcStr, maxLen);
		free(wcFeature);
		checkReturn(env, returnCode);
		
		char *cStr = (char *)malloc((maxLen + 2));
		if(cStr == NULL)
			checkReturn(env, JNI_LAYER_ERROR);
		wcstombs(cStr, wcStr, maxLen);	
		free(wcStr);

		jstring jStr = env->NewStringUTF(cStr);
		if(jStr == NULL)
			checkReturn(env, JNI_LAYER_ERROR);
		return jStr;

	}catch(const std::exception& ex){
		printf("STD EXCEPTION in AT_GetEnumStringByIndex\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);		

	}catch(...){
		printf("EXCEPTION UNKNOWN AT_GetEnumStringByIndex\n");
		checkReturn(env, AT_ERR_DEVICENOTFOUND);
	}
	return NULL;
}

