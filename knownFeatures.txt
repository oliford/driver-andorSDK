AccumulateCount Integer 	
AOIBinning Enumerated 		
AOIHBin Integer 
AOIHeight Integer 
AOILeft Integer 
AOIStride Integer 
AOITop Integer 
AOIVBin Integer 
AOIWidth Integer 
AuxiliaryOutSource Enumerated 
BaselineLevel Integer
BitDepth Enumerated
BufferOverflowEvent Integer
BytesPerPixel Floating Point
CameraAcquiring Boolean  
CameraModel String
ControllerID String
CycleMode Enumerated
DeviceCount Integer 
DeviceVideoIndex Integer
ElectronicShutteringMode Enumerated
EventEnable Boolean
EventsMissedEvent Integer
EventSelector Enumerated
ExposureTime Floating Point
ExposureEndEvent Integer 
ExposureStartEvent Integer 
FanSpeed Enumerated 
FirmwareVersion String 
FrameCount Integer 
FrameRate Floating
FullAOIControl Boolean 
ImageSizeBytes Integer 
IOInvert Boolean 
IOSelector Enumerated 
LUTIndex Integer
LUTValue Integer 
MaxInterfaceTransferRate Float 
MetadataEnable Boolean 
MetadataFrame Boolean 
MetadataTimestamp Boolean 
Overlap Boolean 
PixelCorrection Enumerated
PixelEncoding Enumerated 
PixelHeight Floating Point
PixelReadoutRate Enumerated
PixelWidth Floating Point
PreAmpGain Enumerated
PreAmpGainChannel Enumerated
PreAmpGainControl Enumerated
PreAmpGainSelector Enumerated
ReadoutTime Floating Point
RowNExposureEndEvent Integer
RowNExposureStartEvent Integer 
SensorCooling Boolean
SensorHeight Integer
SensorTemperature Floating Point 
SensorWidth Integer
SerialNumber String
SimplePreAmpGainControl Enumerated   
SoftwareVersion String
SpuriousNoiseFilter Boolean 
Synchronous Triggering Boolean
TargetSensor Temperature Floating Point
Temperature Control Enumerated
Temperature Status Enumerated
TimestampClock Integer
TimestampClock Frequency Integer 
TriggerMode Enumerated	

AcquisitionStart Command 	
AcquisitionStop Command             
CameraDump Command
SoftwareTrigger Command
TimestampClock Reset Command
