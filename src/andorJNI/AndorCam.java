package andorJNI;

import java.nio.ByteBuffer;
import java.util.HashMap;

/** A slightly more object oriented interface to the camera */
public class AndorCam {
	private int handle = AndorSDK.AT_HANDLE_UNINITIALISED;
	
	public AndorCam() {
		System.err.println("AT.InitialiseLibrary()"); System.err.flush();
		AndorSDK.AT_InitialiseLibrary();
	}
	
	public int open(int CameraIndex){
		System.err.println("AT.open()"); System.err.flush();
		handle = AndorSDK.AT_Open(CameraIndex);
		return handle;
	}
	

	//typedef int (AT_EXP_CONV *FeatureCallback)(, String feature, void* Context);	
	//public int AT_RegisterFeatureCallback(, String feature, FeatureCallback EvCallback, void* Context);
	//public int AT_UnregisterFeatureCallback(, String feature, FeatureCallback EvCallback, void* Context);

	public boolean isImplemented(String feature){ return AndorSDK.AT_IsImplemented(handle, feature); }
	public boolean isReadable(String feature){ return AndorSDK.AT_IsReadable(handle, feature); }
	public boolean isWritable(String feature){ return AndorSDK.AT_IsWritable(handle, feature); }
	public boolean isReadOnly(String feature){ return AndorSDK.AT_IsReadOnly(handle, feature); }

	public void setInt(String feature, long value){ AndorSDK.AT_SetInt(handle, feature, value); }
	public long getInt(String feature){ return AndorSDK.AT_GetInt(handle, feature); }
	public long getIntMax(String feature){ return AndorSDK.AT_GetIntMax(handle, feature); }
	public long getIntMin(String feature){ return AndorSDK.AT_GetIntMin(handle, feature); }

	public void setFloat(String feature, double value){ AndorSDK.AT_SetFloat(handle, feature, value); }
	public double getFloat(String feature){ return AndorSDK.AT_GetFloat(handle, feature); }
	public double getFloatMax(String feature){ return AndorSDK.AT_GetFloatMax(handle, feature); }
	public double getFloatMin(String feature){ return AndorSDK.AT_GetFloatMin(handle, feature); }
	
	public void setBool(String feature, boolean value){ AndorSDK.AT_SetBool(handle, feature, value); }
	public boolean getBool(String feature){ return AndorSDK.AT_GetBool(handle, feature); }

	public void setString(String feature, String str){ AndorSDK.AT_SetString(handle, feature, str); }
	public String getString(String feature){ return AndorSDK.AT_GetString(handle, feature); }
	public int getStringMaxLength(String feature){ return AndorSDK.AT_GetStringMaxLength(handle, feature); }
	
	
	public void setEnumIndex(String feature, int value){ AndorSDK.AT_SetEnumIndex(handle, feature, value); }
	public void setEnumString(String feature, String value){ AndorSDK.AT_SetEnumString(handle, feature, value); }
	public int getEnumIndex(String feature){ return AndorSDK.AT_GetEnumIndex(handle, feature); }
	public int getEnumCount(String feature){ return AndorSDK.AT_GetEnumCount(handle, feature); }
	public boolean isEnumIndexAvailable(String feature, int index){ return AndorSDK.AT_IsEnumIndexAvailable(handle, feature, index); }
	public boolean isEnumIndexImplemented(String feature, int index){ return AndorSDK.AT_IsEnumIndexImplemented(handle, feature, index); }
	public String getEnumStringByIndex(String feature, int index){ return AndorSDK.AT_GetEnumStringByIndex(handle, feature, index); }

	
	public void command(String feature){ System.err.println("AT.command()"); System.err.flush(); AndorSDK.AT_Command(handle, feature); }
	
	public void flush(){ System.err.println("AT.flush()"); System.err.flush(); AndorSDK.AT_Flush(handle); }
	
	/* The actual AT_JNIWaitBuffer() returns the low-level direct buffer address, which
	 * isn't very useful to us here in Java.
	 *  
	 * So, the  AT_JNIQueueBuffer() returns the low-level address of the direct buffer for
	 * us which we store in a map of all kept buffers against the some identifying object passed
	 * from the called, which we can then return from AT_WaitBuffer()
	 */
	
	/** Keys are the buffer low-level address, values are given to AT_QueueBuffer() */
	private HashMap<Long, Object> buffersInPlay = new HashMap<Long, Object>();

	/** Queues a buffer and hold an identifying object that will be returned by AT_WaitBuffer() */
	public void queueBuffer(ByteBuffer buff, Object bufferID, int size){
		//System.err.println("AT.queueBuffer()"); System.err.flush();
		long buffAddr = AndorSDK.AT_QueueBuffer(handle, buff, size);
		buffersInPlay.put(buffAddr, bufferID);
	}
	
	/** Waits for a filled buffer to be returned.
	 * @return The object given to AT_QueueBuffer() when the buffer that has been filled was queued */
	public Object waitBuffer(int Timeout){
		//System.err.println("AT.waitBuffer()"); System.err.flush();
		long buffAddr = AndorSDK.AT_WaitBuffer(handle, Timeout);
			
		synchronized (buffersInPlay) {
			Object buffInfo = buffersInPlay.get(buffAddr);
			if(buffInfo == null)
				throw new AndorSDKException(AndorSDKException.BUFFER_NOT_IN_LIST);
			buffersInPlay.put(buffAddr, null);
			return buffInfo;
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		close();
		AndorSDK.AT_FinaliseLibrary();
	}
	
	public void close(){
		if(handle != AndorSDK.AT_HANDLE_UNINITIALISED){
			try{
				AndorSDK.AT_Flush(handle);
			}catch(Throwable err){
				//we don't really care if the flush worked but really need to close it
				System.err.println("AndorCam: Couldn't flush driver on close. Ignoring: " + err);
			}
			AndorSDK.AT_Close(handle);
		}
		
		handle = AndorSDK.AT_HANDLE_UNINITIALISED;
	}
}
