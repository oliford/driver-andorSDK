package andorJNI;

import java.nio.ByteBuffer;
import java.util.HashMap;

import otherSupport.SettingsManager;

/** Direct JNI calls to the Andor SDK */ 
public class AndorSDK {
	static {
		System.load(SettingsManager.defaultGlobal().getPathProperty("andorSDK.jniLibPath", "/usr/lib/andorJNI.so"));
	}

	public static final int AT_INFINITE = 0xFFFFFFFF;

	public static final int AT_CALLBACK_SUCCESS = 0;

	public static final int AT_TRUE = 1;
	public static final int AT_FALSE = 0;

	public static final int AT_HANDLE_UNINITIALISED = -1;
	public static final int AT_HANDLE_SYSTEM = 1;

	public static native void AT_InitialiseLibrary();
	public static native void AT_FinaliseLibrary();
	
	public static native int AT_Open(int CameraIndex); //return handle (int)
	public static native void AT_Close(int Hndl);

	//typedef int (AT_EXP_CONV *FeatureCallback)(int Hndl, String feature, void* Context);	
	//public static native int AT_RegisterFeatureCallback(int Hndl, String feature, FeatureCallback EvCallback, void* Context);
	//public static native int AT_UnregisterFeatureCallback(int Hndl, String feature, FeatureCallback EvCallback, void* Context);

	public static native boolean AT_IsImplemented(int Hndl, String Feature);
	public static native boolean AT_IsReadable(int Hndl, String feature);
	public static native boolean AT_IsWritable(int Hndl, String feature);
	public static native boolean AT_IsReadOnly(int Hndl, String feature);

	public static native void AT_SetInt(int Hndl, String feature, long Value);
	public static native long AT_GetInt(int Hndl, String feature);
	public static native long AT_GetIntMax(int Hndl, String feature);
	public static native long AT_GetIntMin(int Hndl, String feature);

	public static native void AT_SetFloat(int Hndl, String feature, double Value);
	public static native double AT_GetFloat(int Hndl, String feature);
	public static native double AT_GetFloatMax(int Hndl, String feature);
	public static native double AT_GetFloatMin(int Hndl, String feature);

	
	public static native void AT_SetBool(int Hndl, String feature, boolean value);
	public static native boolean AT_GetBool(int Hndl, String feature);

	
	public static native void AT_Command(int Hndl, String feature);
	
	public static native void AT_Flush(int Hndl);
	
	/** @param buff A direct byte buffer 
	 * @return the low level buffer address*/ 
	public static native long AT_QueueBuffer(int Hndl, ByteBuffer buff, int size);
	
	/** Returns the low-level address of the buffer, which has to be checked
	 * against 
	 * @return the low level buffer address */
	public static native long AT_WaitBuffer(int Hndl, int Timeout);
				
	public static native void AT_SetString(int Hndl, String feature, String str);
	public static native String AT_GetString(int Hndl, String feature);
	public static native int AT_GetStringMaxLength(int Hndl, String feature);

/* what?
	public static native int AT_SetEnumerated(int Hndl, String feature, int Value);
	public static native int AT_SetEnumeratedString(int Hndl, String feature, const AT_WC* String);
	public static native int AT_GetEnumerated(int Hndl, String feature, int* Value);
	public static native int AT_GetEnumeratedCount(int Hndl,const  AT_WC* Feature, int* Count);
	public static native int AT_IsEnumeratedIndexAvailable(int Hndl, String feature, int Index, AT_BOOL* Available);
	public static native int AT_IsEnumeratedIndexImplemented(int Hndl, String feature, int Index, AT_BOOL* Implemented);
	public static native int AT_GetEnumeratedString(int Hndl, String feature, int Index, AT_WC* String, int StringLength);
*/
	public static native void AT_SetEnumIndex(int Hndl, String feature, int value);
	public static native void AT_SetEnumString(int Hndl, String feature, String value);
	public static native int AT_GetEnumIndex(int Hndl, String feature);
	public static native int AT_GetEnumCount(int Hndl, String feature);
	public static native boolean AT_IsEnumIndexAvailable(int Hndl, String feature, int index);
	public static native boolean AT_IsEnumIndexImplemented(int Hndl, String feature, int index);
	public static native String AT_GetEnumStringByIndex(int Hndl, String feature, int index);


}
