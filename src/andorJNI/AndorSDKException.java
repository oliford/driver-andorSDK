package andorJNI;

import java.util.HashMap;

public class AndorSDKException extends RuntimeException {
	public static final int AT_SUCCESS = 0;
	public static final int AT_ERR_NOTINITIALISED = 1;
	public static final int AT_ERR_NOTIMPLEMENTED = 2;
	public static final int AT_ERR_READONLY = 3;
	public static final int AT_ERR_NOTREADABLE = 4;
	public static final int AT_ERR_NOTWRITABLE = 5;
	public static final int AT_ERR_OUTOFRANGE = 6;
	public static final int AT_ERR_INDEXNOTAVAILABLE = 7;
	public static final int AT_ERR_INDEXNOTIMPLEMENTED = 8;
	public static final int AT_ERR_EXCEEDEDMAXSTRINGLENGTH = 9;
	public static final int AT_ERR_CONNECTION = 10;
	public static final int AT_ERR_NODATA = 11;
	public static final int AT_ERR_INVALIDHANDLE = 12;
	public static final int AT_ERR_TIMEDOUT = 13;
	public static final int AT_ERR_BUFFERFULL = 14;
	public static final int AT_ERR_INVALIDSIZE = 15;
	public static final int AT_ERR_INVALIDALIGNMENT = 16;
	public static final int AT_ERR_COMM = 17;
	public static final int AT_ERR_STRINGNOTAVAILABLE = 18;
	public static final int AT_ERR_STRINGNOTIMPLEMENTED = 19;

	public static final int AT_ERR_NULL_FEATURE = 20;
	public static final int AT_ERR_NULL_HANDLE = 21;
	public static final int AT_ERR_NULL_IMPLEMENTED_VAR = 22;
	public static final int AT_ERR_NULL_READABLE_VAR = 23;
	public static final int AT_ERR_NULL_READONLY_VAR = 24;
	public static final int AT_ERR_NULL_WRITABLE_VAR = 25;
	public static final int AT_ERR_NULL_MINVALUE = 26;
	public static final int AT_ERR_NULL_MAXVALUE = 27;
	public static final int AT_ERR_NULL_VALUE = 28;
	public static final int AT_ERR_NULL_STRING = 29;
	public static final int AT_ERR_NULL_COUNT_VAR = 30;
	public static final int AT_ERR_NULL_ISAVAILABLE_VAR = 31;
	public static final int AT_ERR_NULL_MAXSTRINGLENGTH = 32;
	public static final int AT_ERR_NULL_EVCALLBACK = 33;
	public static final int AT_ERR_NULL_QUEUE_PTR = 34;
	public static final int AT_ERR_NULL_WAIT_PTR = 35;
	public static final int AT_ERR_NULL_PTRSIZE = 36;
	public static final int AT_ERR_NOMEMORY = 37;
	public static final int AT_ERR_DEVICEINUSE = 38;

	public static final int AT_ERR_HARDWARE_OVERFLOW = 100;
	
	public static final int JNI_LAYER_ERROR = -543;
	public static final int BUFFER_NOT_IN_LIST = -544;
	
	private String functionName;
	private int errorCode;
	
	private static final HashMap<Integer, String> errors = new HashMap<Integer, String>() {{
		 put(1, "NOT INITIALISED"); 
		 put(2, "NOT IMPLEMENTED"); 
		 put(3, "READONLY"); 
		 put(4, "NOT READABLE"); 
		 put(5, "NOT WRITABLE"); 
		 put(6, "OUTOFRANGE"); 
		 put(7, "INDEX NOT AVAILABLE"); 
		 put(8, "INDEX NOT IMPLEMENTED"); 
		 put(9, "EXCEEDED MAX STRING LENGTH"); 
		 put(10, "CONNECTION"); 
		 put(11, "NODATA"); 
		 put(12, "INVALID HANDLE"); 
		 put(13, "TIMEDOUT"); 
		 put(14, "BUFFERFULL"); 
		 put(15, "INVALID SIZE"); 
		 put(16, "INVALID ALIGNMENT"); 
		 put(17, "COMM"); 
		 put(18, "STRING NOT AVAILABLE"); 
		 put(19, "STRING NOT IMPLEMENTED"); 
		 put(20, "NULL FEATURE"); 
		 put(21, "NULL HANDLE"); 
		 put(22, "NULL IMPLEMENTED VAR"); 
		 put(23, "NULL READABLE VAR"); 
		 put(24, "NULL READONLY VAR"); 
		 put(25, "NULL WRITABLE VAR"); 
		 put(26, "NULL MINVALUE"); 
		 put(27, "NULL MAXVALUE"); 
		 put(28, "NULL VALUE"); 
		 put(29, "NULL STRING"); 
		 put(30, "NULL COUNT_VAR"); 
		 put(31, "NULL ISAVAILABLE VAR"); 
		 put(32, "NULL MAXSTRINGLENGTH"); 
		 put(33, "NULL EVCALLBACK"); 
		 put(34, "NULL QUEUE PTR"); 
		 put(35, "NULL WAIT PTR"); 
		 put(36, "NULL PTRSIZE"); 
		 put(37, "NO MEMORY"); 
		 put(38, "DEVICE IN USE"); 
		 put(100, "HARDWARE OVERFLOW");
		 
		 //out JNI layer errors
		 put(-543, "JNI layer error (probably small memory allocation)");
		 put(-544, "WaitBuffer returned a buffer not in the buffers in play list");
	}};
	
	public static final String getErrorString(int returnCode){
			String str = errors.get(returnCode);
			return (str != null) ? str : ("Unknown AndorSDK error '" + returnCode + "'.");			
	}
	
	public AndorSDKException(int errorCode) {
		this("Andor SDK error "+errorCode+": " + getErrorString(errorCode));	
		
		this.functionName = "?";
		this.errorCode = errorCode;		
	}
	
	public AndorSDKException(String functionName, int errorCode) {
		this(functionName + " returned the error "+errorCode+": " + getErrorString(errorCode));	
		
		this.functionName = functionName;
		this.errorCode = errorCode;		
	}
	
	public AndorSDKException(String errorString) { 
		super(errorString);
		this.errorCode = -1;
	}

	public int getErrorCode() { return errorCode; }

	public String getErrorString(){ return getErrorString(errorCode); }			

}
