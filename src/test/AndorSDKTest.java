package test;

import otherSupport.SettingsManager;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import andorJNI.AndorSDK;

/** Basic test of direct access to SDK iva JNI */ 
public class AndorSDKTest {
	static{ new SettingsManager("minerva", true); }
	
	public static void main(String[] args) {
		
		AndorSDK.AT_InitialiseLibrary();
		
		long nDevices = AndorSDK.AT_GetInt(AndorSDK.AT_HANDLE_SYSTEM, "Device Count");
		System.out.println("nDevices = " + nDevices);
		
		int i_handle = AndorSDK.AT_Open(0);
		System.out.println("i_handle = " + i_handle);
		
		String camModel = AndorSDK.AT_GetString(i_handle, "CameraModel"); 
		System.out.println("camModel = " + camModel);
				
		boolean hasAOIHeight = AndorSDK.AT_IsImplemented(i_handle, "AOI Height");
		System.out.println("hasAOIHeight = " + hasAOIHeight);
		
		int nEnum = AndorSDK.AT_GetEnumCount(i_handle, "PixelEncoding");
		System.out.println("nEnum(PixelEncoding) = " + nEnum);
				
		for(int i=0; i < nEnum; i++){
			String enumEntry = AndorSDK.AT_GetEnumStringByIndex(i_handle, "PixelEncoding", i); 
			System.out.println("PixelEncoding["+i+"] = " + enumEntry);
			
		}
		
		AndorSDK.AT_Close(i_handle); 		
	
		AndorSDK.AT_FinaliseLibrary();
	}
}
