package test;

import java.nio.ByteBuffer;
import java.nio.ShortBuffer;


import otherSupport.SettingsManager;
import otherSupport.bufferControl.DirectBufferControl;

import andorJNI.AndorSDK;
import andorJNI.AndorSDKException;

public class Image {

	//************************************
	//*** Data
	//************************************
	
	public static boolean b_verbose = true;
	
	public static String sz_filename = "image.bmp";
	
	public static int i_deviceId = 0;
	
	public static int i_handle = 0;
	
	public static double d_exposureTime = 0.1;
	
	public static int aoiHeight = 0, aoiWidth = 0;
	
	public static int image_max = 2047;
	public static int image_min = 0;
	
	public static int i_minScale = -1;
	public static int i_maxScale = -1;
			
	//************************************
	//*** Functions
	//************************************
	public static void showHelp() {
	  System.out.println("Andor SDK 3 Image Capture Example Program\n"+
	         "\n"+
	         "Usage:\n"+
	         "    image [-?] [-vV] [-e <expTime>] [-f <filename>] [-d <device>]\n"+
	         "          [s <min> <max>]\n"+
	         "\n"+
	         "Synopsis:\n"+
	         "  Captures a single full frame image and saves it to a bitmap.\n"+
	         "\n"+
	         "Arguments:\n"+
	         "  -?             : Show this help\n"+
	         "  -v/-V          : Verbose mode\n"+
	         "  -e <exptime>   : Sets the exposure to the specified float value\n"+
	         "  -f <filename>  : Saves the bitmap to the specified file\n"+
	         "  -d <device>    : Acquires from the device number specified\n"+
	         "  -s <min> <max> : Scales the image to the output palette with\n"+
	         "                   the specified min and max count values. If\n"+
	         "                   unspecified, the max and min of the image is used\n"+
	         "\n");
	}
	
	public static void printParams(){
	  System.out.println("Device Id: " + i_deviceId + "\n" +
			  "Exposure : " + d_exposureTime + "  secs\n" +
	  		  "Filename : " + sz_filename);	  
	}
	
	public static void processArgs(String args[]){
		String sz_current;
		int i = 0;
		int n = args.length;
		// Skip program name;
		i++;
		n--;

		while (n > 0) {
			sz_current = args[i];

			if (!sz_current.startsWith("-")) {
				System.out.println("** Invalid command line at : '" + sz_current + "'");
				showHelp();
				System.exit(-1);
			}

			switch (sz_current.charAt(1)) {
			case '?':
				showHelp();
				break;	  	
			case 'v':
			case 'V':
				b_verbose = true;
				break;
			case 'e':
				if (n > 1) {
					n--;
					i++;
					d_exposureTime = Double.parseDouble(args[i]);
				}
				else {
					System.out.println("No exposure value given");
			  		System.exit(-4);
				}
				break;
			case 'f':
				if (n > 1) {
					n--;
					i++;
					sz_filename = args[i];
				}
				else {
					System.out.println("No filename given");
			  		System.exit(-5);
				}	    
				break;
			case 'd':
				if (n > 1) {
					n--;
					i++;
					i_deviceId = Integer.parseInt(args[i]);
				}
				else {
					System.out.println("No device id given");
			  		System.exit(-4);
				}
				break;
			case 's':
				if (n > 2) {
					n--;
					i++;
					i_minScale = Integer.parseInt(args[i]);
					n--;
					i++;
					i_maxScale = Integer.parseInt(args[i]);    
				}
				else {
					System.out.println("No device id given");
			  		System.exit(-4);
				}
				break;   
			case '\0':
				System.out.println("** Invalid empty option");
				showHelp();
		  		System.exit(-2);
				break;	    
			default:
				System.out.println("** Unknown option : '" + sz_current.charAt(1) + "'\n");
				showHelp();
		  		System.exit(-3);
				break;
			}
			i++;
			n--;
		}
	}
	
	public static void init()
	{
		new SettingsManager("minerva", true);
		AndorSDK.AT_InitialiseLibrary();
		
		if (b_verbose) {
			long i64_deviceCount = AndorSDK.AT_GetInt(AndorSDK.AT_HANDLE_SYSTEM, "Device Count");
			System.out.println("Found " + i64_deviceCount + " Devices.");			
		}
		
		i_handle = AndorSDK.AT_Open(i_deviceId);
	}
	
	public static void shutdown() {
		AndorSDK.AT_Close(i_handle); // Don't check in case not opened.
	
		AndorSDK.AT_FinaliseLibrary();		
	}
	
	public static void updateImageSize()	{
		
		String wsz_featureName = "AOI Width";
		if (!AndorSDK.AT_IsImplemented(i_handle, wsz_featureName)) {
			wsz_featureName = "Sensor Width";	
		}		
		aoiWidth = (int)AndorSDK.AT_GetInt(i_handle, wsz_featureName);		
		
		wsz_featureName = "AOI Height";
		if(!AndorSDK.AT_IsImplemented(i_handle, wsz_featureName)) {
			wsz_featureName = "Sensor Height";	
		}
		aoiHeight = (int)AndorSDK.AT_GetInt(i_handle, wsz_featureName);		
			
	}

	public static void setupAcq()	{
		boolean b_set16bit = false;

		AndorSDK.AT_SetFloat(i_handle, "Exposure Time", d_exposureTime);
		
		if (AndorSDK.AT_IsImplemented(i_handle, "AOI Height") && 
				AndorSDK.AT_IsWritable(i_handle, "AOI Height")) {
				
			aoiHeight = (int)AndorSDK.AT_GetIntMax(i_handle, "AOI Height");
			AndorSDK.AT_SetInt(i_handle, "AOI Height", aoiHeight);	
		}
		
		if(AndorSDK.AT_IsImplemented(i_handle, "AOI Width") &&
				AndorSDK.AT_IsWritable(i_handle, "AOI Width")){
			
			aoiWidth = (int)AndorSDK.AT_GetIntMax(i_handle, "AOI Width");
			AndorSDK.AT_SetInt(i_handle, "AOI Width", aoiWidth);
		}

		if(AndorSDK.AT_IsImplemented(i_handle, "SimplePreAmpGainControl")) {
			try{
				AndorSDK.AT_SetEnumString(i_handle, "SimplePreAmpGainControl", "16-bit (low noise & high well capacity)");
				b_set16bit = true;
			}catch(AndorSDKException err){
				System.err.println("Couldn't set 16-bit (low noise & high well capacity) mode");
				b_set16bit = false;
			}
		}

		if(AndorSDK.AT_IsImplemented(i_handle, "PixelEncoding") &&
				AndorSDK.AT_IsWritable(i_handle, "PixelEncoding")) {
			AndorSDK.AT_SetEnumString(i_handle, "PixelEncoding", b_set16bit ? "Mono16" : "Mono12Packed");	
		}
	
		updateImageSize();		
	}
	
	public static void printAcqSettings()
	{
		long i64_value = 0;

		if (AndorSDK.AT_IsImplemented(i_handle, "AOI Height")) {
			System.out.println("AOI Height = " + 
						AndorSDK.AT_GetInt(i_handle, "AOI Height"));
		}
		
		if (AndorSDK.AT_IsImplemented(i_handle, "AOI Width")) {
			System.out.println("AOI Width = " + 
						AndorSDK.AT_GetInt(i_handle, "AOI Width"));
		}

		if(AndorSDK.AT_IsImplemented(i_handle, "Actual Exposure Time")) {
			System.out.println("Actual Exposure Time = " + 
						AndorSDK.AT_GetFloat(i_handle, "Actual Exposure Time"));
		}
	}

	public static int collectStats(ByteBuffer puc_image, int width, int height, int stride){

		ShortBuffer shortBuff = puc_image.asShortBuffer();
		
		image_max = shortBuff.get(0);
		image_min = shortBuff.get(0);
		long i64_total = 0;
		shortBuff.rewind();

		for (int jj = 0; jj < height; jj++) {
			shortBuff.position((jj * stride)/2);
			
			for (int ii = 0; ii < width; ii++) {
				short ui_current = (short)(shortBuff.get() & 0x07FF);
				if (ui_current < image_min) {
					image_min = ui_current;
				}
				else if (ui_current > image_max) {
					image_max = ui_current;
				}
				i64_total += ui_current;						
			}
		}
		if (b_verbose) {
			System.out.println("Average pixel value = " + (i64_total / (width * height)) +
								"\nMin pixel value     = " + image_min +
								"\nMax pixel value     = " + image_max);  	
		}			
		return 0;
	}
	
	public static void acquire(){ 
		long sizeInBytes = AndorSDK.AT_GetInt(i_handle, "ImageSizeBytes");
		int aoiStride = (int)AndorSDK.AT_GetInt(i_handle, "AOIStride");
		
		if(sizeInBytes >= 0x100000000L)
			throw new IllegalArgumentException("Can't handle > 4GB images");
		
		ByteBuffer puc_image = ByteBuffer.allocateDirect((int)sizeInBytes);
		
		long queuedAddr = AndorSDK.AT_QueueBuffer(i_handle, puc_image, (int)sizeInBytes);	
		AndorSDK.AT_Command(i_handle, "Acquisition Start");
		
		int i64_bufSize = 0;
		
		int ui_timeout = (int)(3 * d_exposureTime * 1000);
		if (ui_timeout < 500) {
			ui_timeout = 500;
		}
		
		long doneAddr = AndorSDK.AT_WaitBuffer(i_handle, ui_timeout);
		
		if(doneAddr != queuedAddr)
			throw new RuntimeException( "Returned buffer not equal to queued buffer");
		
		System.out.println("All good!");
		collectStats(puc_image, aoiWidth, aoiHeight, aoiStride);
		if (i_minScale < 0) {
		  i_minScale = image_min;
		  i_maxScale = image_max;
		}
		//saveAsBmp(sz_filename, puc_returnBuf, i64_aoiWidth, i64_aoiHeight, i64_aoiStride, i_minScale, i_maxScale);
	
		DirectBufferControl.freeBuffer(puc_image);		
	}

	public static void main(String[] args) {		

		processArgs(args);

		if (b_verbose)
			printParams();


		init();
		setupAcq();	    
		if (b_verbose) 
			printAcqSettings();

		acquire();
		shutdown();
	}
}
