package test;

import otherSupport.SettingsManager;
import andorJNI.AndorCam;
import andorJNI.AndorSDK;

/** Test camera access through object type SDK */
public class AndorCamTest {
	static{ new SettingsManager("minerva", true); }
	
	public static void main(String[] args) {
		

		AndorCam cam = new AndorCam();
		
		long nDevices = AndorSDK.AT_GetInt(AndorSDK.AT_HANDLE_SYSTEM, "Device Count");
		System.out.println("nDevices = " + nDevices);
		
		int i_handle = cam.open(0);
		System.out.println("i_handle = " + i_handle);
		
		String camModel = cam.getString("CameraModel"); 
		System.out.println("camModel = " + camModel);
				
		boolean hasAOIHeight = cam.isImplemented("AOI Height");
		System.out.println("hasAOIHeight = " + hasAOIHeight);
		
		cam.close(); 		
	
	}
}
